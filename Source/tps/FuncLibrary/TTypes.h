// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "TTypes.generated.h"

UENUM(BlueprintType)
enum class EMovementState : unit8
{
	Aim_State UMETA(DisplayName = "Aim State"),
	Walk_State UMETA(DisplayName = "Walk State"),
	Run_State UMETA(DisplayName = "Run State"),
};

UCLASS()
class tps_API UTTypes : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
};